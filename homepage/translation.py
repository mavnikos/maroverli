from modeltranslation.translator import translator, TranslationOptions

from .models import Block


class BlockTranslate(TranslationOptions):
    fields = ("title",)


translator.register(Block, BlockTranslate)
