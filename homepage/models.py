from django.db import models

from filer.fields.image import FilerImageField


class Block(models.Model):
    title = models.CharField(
        "Header",
        max_length=40,
        blank=True,
        null=True,
        help_text="Text over image",
    )
    image = FilerImageField(
        verbose_name="Image",
        on_delete=models.CASCADE,
        help_text="Recommended max dimensions: <strong>500x1000px</strong>.",
    )
    category = models.ForeignKey("blog.Category", on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField()

    class Meta:
        ordering = ["order"]
