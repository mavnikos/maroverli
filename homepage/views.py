from django.shortcuts import render
from django.utils.translation import gettext as _

from homepage.models import Block


def index(request):
    blocks = Block.objects.all()
    meta_description = _("Maro Verli is an awarded photographer & "
                         "communication expert with a focus on humanitarian "
                         "visual storytelling and a passion for food "
                         "photography.")
    return render(request, "homepage/index.html", {
        "blocks": blocks,
        "page_title": _("Homepage"),
        "meta_description": meta_description,
    })
