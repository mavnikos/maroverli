from django.contrib import admin
from django.utils.html import format_html

from adminsortable2.admin import SortableAdminMixin

from .models import Block


@admin.register(Block)
class BlockAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ("show_thumbnail", "title", "category")
    fields = (("title_en", "title_el"), "category", "image")
    readonly_fields = ("show_thumbnail",)

    def show_thumbnail(self, obj):
        src = obj.image.url
        return format_html(f"<img src={src} height=100px>")

    show_thumbnail.short_description = "Preview"
