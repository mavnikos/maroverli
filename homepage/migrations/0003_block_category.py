# Generated by Django 2.2.9 on 2020-01-13 15:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_auto_20200113_1728'),
        ('homepage', '0002_auto_20191228_2147'),
    ]

    operations = [
        migrations.AddField(
            model_name='block',
            name='category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='blog.Category'),
            preserve_default=False,
        ),
    ]
