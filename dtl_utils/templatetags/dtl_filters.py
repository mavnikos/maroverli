from django import template

register = template.Library()


@register.filter
def alter_lang(value, lang_code):
    if value is not None:
        link_lang_code = value[1:3]
        return value.replace(f"/{link_lang_code}", f"/{lang_code}")
    return value
