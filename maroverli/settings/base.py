from json import loads
from pathlib import Path

from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _


author = "nick"


settings_dir = Path(__file__).resolve().parent
ROOT = settings_dir.parent.parent
secrets = loads(settings_dir.joinpath("secret.json").read_text())


def get_secret(setting, secret=secrets):
    """Get the secret variable or return explicit exception."""
    try:
        return secret[setting]
    except KeyError:
        error_msg = 'Set the {0} environment variable'.format(setting)
        raise ImproperlyConfigured(error_msg)


PROJECT_NAME = 'maroverli'
ROOT_URLCONF = 'maroverli.urls'
WSGI_APPLICATION = 'maroverli.wsgi.application'
SITE_ID = 1


# APP CONFIGURATION
MODELTRANSLATION_APP = ['modeltranslation', ]

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'django.contrib.sitemaps',
    "django.contrib.redirects",
]

THIRD_PARTY_APPS = [
    "mptt",
    "django_jinja",
    "easy_thumbnails",
    "django_countries",
    "adminsortable2",
    "django_select2",
    "django_summernote",
    'compressor',
]

PROJECT_APPS = [
    "dtl_utils.apps.DtlUtilsConfig",
    "homepage.apps.HomepageConfig",
    "blog.apps.BlogConfig",
    "travel_map.apps.TravelMapConfig",
    "about.apps.AboutConfig",
    "media_publicity.apps.MediaPublicityConfig",
    "filer",  # at the end in order for blog app to override the html templates
]

INSTALLED_APPS = (
    MODELTRANSLATION_APP + DJANGO_APPS + THIRD_PARTY_APPS + PROJECT_APPS
)

# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    "django.contrib.redirects.middleware.RedirectFallbackMiddleware",
]


# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = get_secret('SECRET_KEY')

TIME_ZONE = 'Europe/Athens'
LANGUAGE_CODE = 'en'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Paths which include locale directories (used for translation)
LOCALE_PATHS = (
    ROOT.joinpath("maroverli/locale"),
)

# Preferred languages of the site
LANGUAGES = (
    ('en', _('English')),
    ('el', _('Greek')),
)


TEMPLATES = [
    {
        'BACKEND': 'django_jinja.backend.Jinja2',
        'DIRS': [
            ROOT.joinpath("templates"),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            # render with Jinja2 all templates under DIRS that end with .html
            'match_extension': '.html',
            # do not use Jinja2 to render any admin, summernote or sitemap html pages
            # these are not urls patterns (they are html filename)
            'match_regex': r'^(?!admin|registration|django_summernote|sitemap).*',
            # enable i18n with new style, which means that instead of this
            # (invalid):
            # {{ _('Hello %(w)s!')|format(w='world') }}
            # we can do this
            # (valid):
            # {{ _('Hello %(w)s!', w='world') }}
            'newstyle_gettext': True,
            'extensions': [
                'jinja2.ext.i18n',
                'django_jinja.builtins.extensions.CsrfExtension',
                'django_jinja.builtins.extensions.CacheExtension',
                'django_jinja.builtins.extensions.TimezoneExtension',
                'django_jinja.builtins.extensions.UrlsExtension',
                'django_jinja.builtins.extensions.StaticFilesExtension',
                'django_jinja.builtins.extensions.DjangoFiltersExtension',
                'compressor.contrib.jinja2ext.CompressorExtension',
            ],
            # [Available settings]
            # github.com/niwinz/django-jinja/blob/master/django_jinja/
            # backend.py#L157
            # [Usage]
            # github.com/niwinz/django-jinja/blob/master/django_jinja/builtins
            # /extensions.py#L99
            'bytecode_cache': {
                'enabled': True
            },
            # values here must be NOT callables, just a simple key:value pair.
            'constants': {
                'SITE_TITLE': _('Maro Verli'),
                "MARO_NAME": _("Maro Verli"),
                "MARO_TEL": _("+30 6972 123 456"),
                "MARO_EMAIL": "info@maroverli.com",
                "MARO_FB": "https://www.facebook.com/maro.ver.1",
                "MARO_INSTA": "https://www.linkedin.com/in/maro-maria-verli-a2b6967b/",
                "MARO_LI": "https://instagram.com/maro.verli",
            },
            # values here MUST be 'path.to.modules' or 'path.to.functions'.
            'globals': {
                'messages': 'django.contrib.messages.api.get_messages',
                'translation': 'django.utils.translation',
                'settings': 'django.conf.settings',
                'lang_info': 'maroverli.jinja2.methods.lang_info',
                'thumbnail': 'maroverli.jinja2.methods.thumbnail',
            },
            # values here MUST be path to a callable (i.e function)
            'filters': {
                'remove_accent': 'maroverli.jinja2.filters.remove_accent',
            },
        },
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [
            ROOT.joinpath("templates"),
            ROOT.joinpath("templates/sitemap"),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
        },
    },
]

FILE_UPLOAD_PERMISSIONS = 0o644
MEDIA_URL = '/media-file/'
MEDIA_ROOT = ROOT / "media_root"
STATIC_URL = '/static/'
STATIC_ROOT = ROOT / "static_root"
STATICFILES_DIRS = (
    # CSS files
    ('css', ROOT.joinpath("static/css")),

    # Fonts
    ('fonts', ROOT.joinpath("static/fonts")),

    # Files
    ('files', ROOT.joinpath("static/files")),

    # JS files
    ('js', ROOT.joinpath("static/js")),

    # Images
    ('img', ROOT.joinpath("static/img")),
)


# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


def get_jinja2_env():
    from django_jinja.backend import Jinja2
    return Jinja2.get_default().env


COMPRESS_JINJA2_GET_ENVIRONMENT = get_jinja2_env


# VIMAGE = {
#     'blog.models.Post.thumbnail': {
#         'DIMENSIONS': {
#             'lte': (600, 600),
#             'gte': (300, 300),
#         },
#     }
# }
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

FILER_ALLOW_REGULAR_USERS_TO_ADD_ROOT_FOLDERS = True
FILER_PAGINATE_BY = 100

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    # 'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# CRITICAL --> ERROR --> WARNING --> INFO --> DEBUG
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'production_only': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'development_only': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'simple': {
            'format': '[%(asctime)s] %(levelname)s %(message)s',
            'datefmt': '%m/%d/%Y %H:%M:%S',
        },
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s',
            'datefmt': '%m/%d/%Y %H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['development_only'],
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['production_only'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
        'db_logfile': {
            'level': 'WARNING',
            'filters': ['production_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': ROOT.joinpath("logs/db.log"),
            'maxBytes': 1024 * 1024,
            'backupCount': 3,
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.db.backends': {
            'handlers': ['db_logfile'],
            'level': 'WARNING',
            'propagate': False,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        # `email` is an explicit logger which will send email to ADMINS,
        # ONLY if the logger inside the module
        # (i.e email_logger = logging.getLogger('email')) will be called
        # with error (i.e email_logger.error(...))
        'email': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False
        },
        # if a log of level INFO (or greater) occurred in the `cart` app,
        # log it in logs/my_apps.log
        # 'cart': {
        #    'handlers': ['my_apps'],
        #    'level': 'INFO',
        # },
        # if a log of level INFO (or greater) occurred in the `info` app,
        # log it in logs/my_apps.log & send emails
        # 'info': {
        #    'handlers': ['my_apps', 'mail_admins'],
        #    'level': 'INFO',
        #    'propagate': False
        # },
    }
}

