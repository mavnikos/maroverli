from django.urls import path, re_path, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.conf import settings

from about import views as about_views
from homepage import views as homepage_views
from blog import views as blog_views
from travel_map import views as travel_views
from media_publicity import views as media_publicity_views
from blog.models import Category
from .sitemap import SITEMAPS


def generate_category_urls():
    category_slugs = Category.objects.values_list("slug", flat=True)
    paths = []
    for slug in category_slugs:
        if slug == "blog":
            continue
        url = f"{slug}/"
        if slug == "portfolio":
            url = ""
        paths.append(
            path(url, blog_views.index, {"category": slug}, name=slug)
        )
    return paths


category_urls = generate_category_urls()


urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
    path("summernote/", include("django_summernote.urls")),
    path("filer/", include("filer.urls")),
]

urlpatterns += i18n_patterns(
    path("%s/" % settings.MY_ADMIN_URL, admin.site.urls),
    path("", homepage_views.index, name="homepage"),
    path("article/<slug:post_slug>/", blog_views.article, name="article"),
    path("portfolio/", include(category_urls)),
    path("blog/", blog_views.index, {"category": "blog"}, name="blog"),
    path("travel-map/", travel_views.index, name="travel_map"),
    path("about-me/", about_views.index, name="about"),
    path(
        "media-publicity/", media_publicity_views.index, name="media_publicity"
    ),
)

# Uncomment below to support sitemap
urlpatterns += [
   re_path(r"^sitemap\.xml/$",
           sitemap,
           {"sitemaps": SITEMAPS, "template_name": "sitemap.xml"},
           name="django.contrib.sitemaps.views.sitemap"
           )
]

# Uncomment below to serve uploaded files and custom error pages
if settings.DEBUG:
    from django.views.static import serve
    from django.views.defaults import page_not_found, server_error

    urlpatterns += [
        re_path(
            r"^media-file/(?P<path>.*)",
            serve,
            {"document_root": settings.MEDIA_ROOT},
        ),
        re_path(
            r"^favicon/(?P<path>.*)",
            serve,
            {"document_root": settings.ROOT.joinpath("favicon")},
        ),
    ]

    urlpatterns += [
        path("404/", page_not_found, {"exception": ""}),
        path("500/", server_error),
    ]

# path("portfolio/", include([
#     path("", blog_views.portfolio, {"category": "portfolio"}, name="portfolio"),
#     path("features/", blog_views.portfolio, {"category": "features"}, name="features"),
#     path("food/", blog_views.portfolio, {"category": "food"}, name="food"),
#     path("special-projects/", blog_views.portfolio, {"category": "special-projects"}, name="special-projects"),
# ])),
