from django.contrib import sitemaps
from django.urls import reverse

from blog.models import Category, Post


class BlogCategorySitemap(sitemaps.Sitemap):
    changefreq = "monthly"
    priority = 0.6
    i18n = True

    def items(self):
        return Category.objects.all()


class BlogPostSitemap(sitemaps.Sitemap):
    changefreq = "monthly"
    priority = 0.6
    i18n = True

    def items(self):
        return Post.active_objects.all()

    def lastmod(self, obj):
        return obj.updated_at


class StaticViewSitemap(sitemaps.Sitemap):
    changefreq = "yearly"
    priority = 0.5
    i18n = True

    def items(self):
        return ["travel_map", "about", "media_publicity"]

    def location(self, obj):
        return reverse(obj)


SITEMAPS = {
    "blog_category": BlogCategorySitemap,
    "blog_post": BlogPostSitemap,
    "static": StaticViewSitemap
}
