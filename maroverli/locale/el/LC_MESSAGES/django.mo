��    :      �  O   �      �     �     
                          0  	   2     <     H  
   [     f  �   }                -  
   /     :     B     P     a     j     z     �     �     �     �  �   �     T     c  	   u          �     �     �     �  
   �  �   �     I     Y     i     y  	   {     �     �     �     �     �     �  �   �  
   Y	  �   d	     
     
     
     
     %
  k  6
     �     �     �     �     �     �     �     �       )   $     N  A   j    �     �     �     �     �               0     F  3   [  %   �  3   �  .   �          )  �   6     '     D     [     u     �     �     �  
   �     �  9  �  $     5   ;     q     �     �      �  /   �  #   �          $     '  �   =  %       E     [  (   ^     �     �  (   �     $      +   ,   )      3                    1      6      .         (      2                           0                8       5      :   !   9   /           *      #   4          '   -          7   &           "       %                              	                          
       +30 6972 123 456 A ALL About Me Accept Active language B Change to Choose File Click to open menu Close Menu Customize your website Dare to Dream Big! Cause Every Big dream is a new goal and every difficulty, an opportunity to learn. Here is my life story! Available for new challenges! Decline Developed by E Email icon English Facebook icon Facebook profile Features Filter posts by Food Photography Go to Homepage Go to Portfolio page Greek Homepage If you have a photo request, send me an email or have a look here for media coverage, interviews, press & social media mentions, and other shoutouts. Instagram icon Instagram profile Languages Linkedin icon Linkedin profile Lookup M MENU Maro Verli Maro Verli is an awarded photographer & communication expert with a focus on humanitarian visual storytelling and a passion for food photography. Maro Verli logo Maroverli Admin Media Publicity O Published Read See all posts Send me an email! Special Projects T Terms of use This website uses <a href='https://policies.google.com/technologies/cookies'>cookies</a> to ensure you get the best experience on our website. Travel Map Traveled in 30 countries & overnight in many airports. Traveling is an opportunity for self-development by exploring your limits & being open to the unknown. U View on site file missing letter no file selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-01-18 08:53+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 6972 123 456 Σ ΟΛΑ Σχετικά Αποδοχή Ενεργή γλώσσα Χ Αλλαγή σε Επιλογή αρχείου Κλικ για άνοιγμα μενού Κλείσιμο μενού Παραμετροποίησε την ιστοσελίδα σου Τολμήσε να ονειρευτείς χωρίς όριο! Κάθε μεγάλο όνειρο είναι ένας νέος στόχος και κάθε δυσκολία, μια ευκαιρία για να μάθω. Αυτό δείχνει η δική μου ιστορία! Άρνηση Ανάπτυξη από Α Εικονίδιο email Αγγλικά Εικονίδιο facebook Προφίλ facebook Αφιερώματα Δείτε αναρτήσεις σχετικά με Φωτογράφιση Φαγητού Μετάβαση στην αρχική σελίδα Μετάβαση στη σελίδα Portfolio Ελληνικά Αρχική Αν ενδιαφέρεστε για φωτογράφιση στείλτε μου ένα email ή βρείτε εδώ συνεντεύξεις, αναφορές στον Τύπο και τα κοινωνικά μέσα ενημέρωσης. Εικονίδιο instagram Προφίλ instagram Ξένες γλώσσες Εικονίδιο linkedin Προφιλ linkedin Αναζήτηση Κ ΜΕΝΟΥ Μάρω Βερλή Η Μάρω Βερλή είναι βραβευμένη φωτογράφος & ειδική στην επικοινωνία. Το πορτφόλιο της αφορά κυρίως αφιερώματα σε ανθρωπιστικές κρίσεις, ενώ αγαπάει την φωτογραφία φαγητού. Λογότυπο Μάρω Βερλή Διαχείριση ιστοσελίδας maroverli Media Publicity Ε Δημοσίευση Διαβάστε το άρθρο Δείτε όλες τις αναρτήσεις Στείλτε μου ένα email! Ειδικά Projects Ι Όροι Χρήσης Η ιστοσελίδα χρησιμοποιεί μηχανισμό <a href='https://policies.google.com/technologies/cookies'>cookies</a> με σκοπό να επιτύχει την καλύτερη πλοήγησή σας. Ταξιδιωτικός χάρτης Έχω ταξιδέψει σε 30 χώρες & έχω κοιμηθεί σε πολλά αεροδρόμια. Ταξιδεύοντας μαθαίνεις τα όρια σου & εξελίσσεσαι προσωπικά. Να ταξιδεύεις σημαίνει να ζεις.  Τ Προβολή στον ιστότοπο το αρχείο λείπει γράμμα δεν επιλέχτηκε αρχείο 