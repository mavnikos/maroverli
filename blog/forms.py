from django import forms

from filetype import guess
from filetype.types.archive import Pdf


class PostAdminForm(forms.ModelForm):
    def clean_pdf_file(self):
        data = self.cleaned_data["pdf_file"]
        if data is not None:
            t = guess(data.file)
            if not isinstance(t, Pdf):
                raise forms.ValidationError('Only PDF file types allowed!')
        return data
