# Generated by Django 2.2.9 on 2020-02-07 19:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_auto_20200206_1357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='title',
            field=models.CharField(blank=True, help_text='Maximum 80 characters.', max_length=80, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='post',
            name='title_el',
            field=models.CharField(blank=True, help_text='Maximum 80 characters.', max_length=80, null=True, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='post',
            name='title_en',
            field=models.CharField(blank=True, help_text='Maximum 80 characters.', max_length=80, null=True, verbose_name='Title'),
        ),
    ]
