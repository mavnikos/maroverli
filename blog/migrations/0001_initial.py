# Generated by Django 2.2.8 on 2019-12-16 20:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Maximum 80 characters.', max_length=80, unique=True, verbose_name='Name')),
                ('name_en', models.CharField(help_text='Maximum 80 characters.', max_length=80, null=True, unique=True, verbose_name='Name')),
                ('name_el', models.CharField(help_text='Maximum 80 characters.', max_length=80, null=True, unique=True, verbose_name='Name')),
                ('slug', models.SlugField(max_length=80, unique=True)),
            ],
            options={
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='Maximum 80 characters.', max_length=80, unique=True, verbose_name='Title')),
                ('title_en', models.CharField(help_text='Maximum 80 characters.', max_length=80, null=True, unique=True, verbose_name='Title')),
                ('title_el', models.CharField(help_text='Maximum 80 characters.', max_length=80, null=True, unique=True, verbose_name='Title')),
                ('slug', models.SlugField(max_length=100, unique=True)),
                ('content', models.TextField(verbose_name='Post content')),
                ('content_en', models.TextField(null=True, verbose_name='Post content')),
                ('content_el', models.TextField(null=True, verbose_name='Post content')),
                ('is_active', models.BooleanField(default=True, verbose_name='Active post?')),
                ('short_content', models.TextField(blank=True, help_text='Maximum 300 characters.', max_length=300, null=True, verbose_name='Short Description')),
                ('short_content_en', models.TextField(blank=True, help_text='Maximum 300 characters.', max_length=300, null=True, verbose_name='Short Description')),
                ('short_content_el', models.TextField(blank=True, help_text='Maximum 300 characters.', max_length=300, null=True, verbose_name='Short Description')),
                ('alt_text', models.CharField(max_length=150, verbose_name='Alternate text')),
                ('alt_text_en', models.CharField(max_length=150, null=True, verbose_name='Alternate text')),
                ('alt_text_el', models.CharField(max_length=150, null=True, verbose_name='Alternate text')),
                ('order', models.SmallIntegerField(default=0)),
                ('meta_description', models.TextField(blank=True, help_text='Maximum 160 characters.', max_length=160, null=True)),
                ('meta_description_en', models.TextField(blank=True, help_text='Maximum 160 characters.', max_length=160, null=True)),
                ('meta_description_el', models.TextField(blank=True, help_text='Maximum 160 characters.', max_length=160, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('categories', models.ManyToManyField(to='blog.Category')),
            ],
            options={
                'ordering': ['order', 'updated_at'],
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Maximum 80 characters.', max_length=80, unique=True, verbose_name='Name')),
                ('name_en', models.CharField(help_text='Maximum 80 characters.', max_length=80, null=True, unique=True, verbose_name='Name')),
                ('name_el', models.CharField(help_text='Maximum 80 characters.', max_length=80, null=True, unique=True, verbose_name='Name')),
                ('slug', models.SlugField(max_length=80, unique=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PostImageDeposit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.SmallIntegerField(default=0)),
                ('alt_text', models.CharField(blank=True, max_length=150, null=True, verbose_name='Alternate text')),
                ('alt_text_en', models.CharField(blank=True, max_length=150, null=True, verbose_name='Alternate text')),
                ('alt_text_el', models.CharField(blank=True, max_length=150, null=True, verbose_name='Alternate text')),
                ('large_img', filer.fields.image.FilerImageField(help_text='Recommended max dimensions: <strong>1920x1080px</strong>.', on_delete=django.db.models.deletion.CASCADE, to=settings.FILER_IMAGE_MODEL, verbose_name='Large image')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='blog.Post')),
            ],
            options={
                'verbose_name': "post's large image",
                'verbose_name_plural': "post's large images",
                'ordering': ['order', 'post__updated_at'],
            },
        ),
        migrations.AddField(
            model_name='post',
            name='tags',
            field=models.ManyToManyField(to='blog.Tag'),
        ),
        migrations.AddField(
            model_name='post',
            name='thumbnail',
            field=filer.fields.image.FilerImageField(help_text='Recommended max dimensions: <strong>600x600px</strong>.', on_delete=django.db.models.deletion.CASCADE, to=settings.FILER_IMAGE_MODEL, verbose_name='Thumbnail'),
        ),
    ]
