# Generated by Django 2.2.12 on 2020-05-26 20:20

from django.conf import settings
from django.db import migrations
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('blog', '0019_auto_20200426_1956'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='social_photo',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Photo for social media. Minimum width 600px. Recommended 1920px.', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='social_photo', to=settings.FILER_IMAGE_MODEL),
        ),
    ]
