import random
import string

from django.db import models
from django.utils.text import slugify
from django.utils.translation import override
from django.urls import reverse
from django.contrib.staticfiles.templatetags.staticfiles import static

from filer.fields.image import FilerImageField

SLUG_TIMELINE_PREFIX = "timeline-"


def random_string(k=15):
    return "".join(random.choices(string.ascii_lowercase + string.digits, k=k))


class ManyToMany(models.Model):
    name = models.CharField(
        max_length=80, unique=True, help_text="Maximum 80 characters."
    )
    slug = models.SlugField(max_length=80, unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        with override(language="en"):
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class Category(ManyToMany):
    header = models.TextField(blank=True)
    social_photo = FilerImageField(
        on_delete=models.CASCADE,
        related_name="social_photo",
        blank=True,
        null=True,
        help_text="Photo for social media. Minimum width 600px. "
                  "Recommended 1920px.",
    )
    meta_description = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = "categories"

    def get_absolute_url(self):
        return reverse(self.slug)


class Tag(ManyToMany):
    pass


class ActivePostManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)


class Post(models.Model):
    title = models.CharField(
        "Title", max_length=80, blank=True, help_text="Maximum 80 characters."
    )
    slug = models.SlugField(max_length=100, unique=True, blank=True)
    content = models.TextField("Post content", blank=True)
    is_active = models.BooleanField("Active post?", default=True)
    tags = models.ManyToManyField(Tag, blank=True)
    categories = models.ManyToManyField(Category)
    short_content = models.TextField(
        "Short Description",
        max_length=300,
        blank=True,
        help_text="Maximum 300 characters.",
    )
    thumbnail = FilerImageField(
        verbose_name="Thumbnail",
        on_delete=models.CASCADE,
        related_name="thumbnail",
        help_text="Recommended max dimensions: <strong>600x600px</strong>.",
    )
    alt_text = models.CharField("Alternate text", max_length=150, blank=True)
    order = models.SmallIntegerField(default=0)
    meta_description = models.TextField(
        max_length=160, blank=True, help_text="Maximum 160 characters."
    )
    background_image = FilerImageField(
        verbose_name="Background large image",
        on_delete=models.CASCADE,
        related_name="background_image",
        blank=True,
        null=True,
        help_text="Recommended max dimensions: <strong>1920x1080px</strong>.",
    )
    pdf_file = models.FileField(verbose_name="PDF file", blank=True, null=True)
    created_at = models.DateField("Created at")
    updated_at = models.DateField("Updated at")
    objects = models.Manager()
    active_objects = ActivePostManager()

    class Meta:
        ordering = ["-created_at"]

    def __str__(self):
        return self.title

    @staticmethod
    def create_slug(s):
        slug = slugify(s)
        if not slug:
            slug = random_string()
        return slug

    def save(self, *args, **kwargs):
        if self.slug:
            slug = self.create_slug(self.slug)
        elif self.title_en:
            slug = self.create_slug(self.title_en)
        elif self.title_el:
            slug = self.create_slug(self.title_el)
        else:
            slug = random_string()
        self.slug = slug
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("article", kwargs={"post_slug": self.slug})

    def permalink(self):
        if self.pk:
            return self.get_absolute_url()[3:]  # strip out "/en" or "/el"
        return ""

    def tags_as_csv(self):
        return ", ".join(self.tags.values_list("slug", flat=True))


class PostImageDeposit(models.Model):
    post = models.ForeignKey(
        Post, related_name="images", on_delete=models.CASCADE
    )
    large_img = FilerImageField(
        verbose_name="Large image",
        on_delete=models.CASCADE,
        help_text="Recommended max dimensions: <strong>1920x1080px</strong>.",
    )
    order = models.SmallIntegerField(default=0)
    alt_text = models.CharField("Alternate text", max_length=150, blank=True)

    class Meta:
        verbose_name = "post's large image"
        verbose_name_plural = "post's large images"
        ordering = ["order", "post__updated_at"]

    def __str__(self):
        return self.post.title


class PostTimeline(models.Model):
    title = models.CharField(
        "Title", max_length=80, blank=True, help_text="Maximum 80 characters."
    )
    slug = models.SlugField(max_length=100, unique=True, blank=True)
    thumbnail = FilerImageField(
        verbose_name="Thumbnail",
        on_delete=models.CASCADE,
        related_name="timeline_thumbnail",
        help_text="Recommended max dimensions: <strong>600x600px</strong>.",
    )
    alt_text = models.CharField(
        "Alternate text",
        max_length=150,
        blank=True,
        help_text="Alternate text for thumbnail.",
    )
    meta_description = models.TextField(
        max_length=160, blank=True, help_text="Maximum 160 characters."
    )
    tags = models.ManyToManyField(Tag, blank=True)
    categories = models.ManyToManyField(Category)

    created_at = models.DateField("Created at")
    updated_at = models.DateField("Updated at")

    @staticmethod
    def create_slug(s):
        slug = slugify(s)
        if not slug:
            slug = random_string()
        return slug

    def save(self, *args, **kwargs):
        if self.slug:
            slug = self.create_slug(self.slug)
        elif self.title_en:
            slug = self.create_slug(self.title_en)
        elif self.title_el:
            slug = self.create_slug(self.title_el)
        else:
            slug = random_string()
        if not slug.strip().startswith(SLUG_TIMELINE_PREFIX):
            slug = f"{SLUG_TIMELINE_PREFIX}{slug}"
        self.slug = slug
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("article", kwargs={"post_slug": self.slug})

    def tags_as_csv(self):
        return ", ".join(self.tags.values_list("slug", flat=True))

    class Meta:
        ordering = ["updated_at"]


class PostTimelineEntry(models.Model):
    post = models.ForeignKey(
        PostTimeline, related_name="timeline_entries", on_delete=models.CASCADE
    )
    heading = models.CharField(
        max_length=80, blank=True, help_text="Maximum 80 characters."
    )
    content = models.TextField(blank=True)
    image = FilerImageField(
        verbose_name="Image",
        on_delete=models.CASCADE,
        related_name="image",
        blank=True,
        null=True,
        help_text="Recommended max dimensions: <strong>600x600px</strong>.",
    )
    icon = models.CharField(
        max_length=30,
        help_text="Go to <a href='https://feathericons.com'>feather icons</a> "
        "and enter the name of the icon you want.<br>Example: if "
        "you want the alert-circle icon, simply write alert-circle.",
    )
    alt_text = models.CharField("Alternate text", max_length=150, blank=True)
    order = models.SmallIntegerField(default=0)

    created_at = models.DateField("Created at")
    updated_at = models.DateField("Updated at")

    def __str__(self):
        return self.heading

    def icon_path(self):
        return static(f"img/icons/svg/{self.icon.strip()}.svg")

    class Meta:
        ordering = ["order", "updated_at"]
