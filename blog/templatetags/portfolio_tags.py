from django_jinja import library


@library.global_function
@library.render_with("blog/pagination.html")
def show_pagination(items, adjacent_pages=2):
    """
    Credits go to: https://www.tummy.com/articles/django-pagination/

    Creates a pagination with ellipsis to help user experience (*x* is the current page).
    For example: 1 2 3 4 5 *6* ... 12 | 1 2 *3* 4 5 6 ... 12 | 1 ... 9 10 *11* 12 | 1 ... 4 5 6 *7* 8 9 10 ... 12
    """
    current_page = items.number
    num_of_pages = items.paginator.num_pages
    start_page = max(current_page - adjacent_pages, 1)
    if start_page <= 3:
        start_page = 1
    end_page = current_page + adjacent_pages + 1
    if end_page >= num_of_pages - 1:
        end_page = num_of_pages + 1
    page_numbers = [
        n for n in range(start_page, end_page) if 0 < n <= num_of_pages
    ]
    return {
        "items": items,
        "current_page": current_page,
        "num_of_pages": num_of_pages,
        "page_numbers": page_numbers,
        "next": items.next_page_number,
        "previous": items.previous_page_number,
        "has_next": items.has_next,
        "has_previous": items.has_previous,
        "show_first": 1 not in page_numbers,
        "show_last": num_of_pages not in page_numbers,
    }
