from itertools import chain

from django.shortcuts import (
    render,
    get_object_or_404,
    get_list_or_404,
    redirect,
)
from django.db.models import Q
from django.utils.translation import get_language

from .models import Category, Tag, Post, PostTimeline, SLUG_TIMELINE_PREFIX


def index(request, category):
    cat = get_object_or_404(Category, slug=category)
    lang_code = get_language()
    common_tag_query = Q(post__categories__slug__in=[category])
    query = {f"title_{lang_code}__isnull": False}
    posts = get_list_or_404(
        Post, is_active=True, categories__slug=category, **query
    )
    timelines = PostTimeline.objects.filter(categories__slug=category, **query)
    tags = Tag.objects.filter(common_tag_query).distinct()
    if timelines.exists():
        posts = sorted(chain(posts, timelines), key=lambda o: o.updated_at)
        tags = Tag.objects.filter(
            Q(posttimeline__categories__slug__in=[category]) | common_tag_query
        ).distinct()
    return render(
        request,
        "blog/index.html",
        {
            "category": cat,
            "posts": posts,
            "tags": tags,
            "page_title": cat.name,
        },
    )


def article(request, post_slug):
    if post_slug.startswith(SLUG_TIMELINE_PREFIX):
        post = get_object_or_404(PostTimeline, slug=post_slug)
        template = "blog/article_timeline.html"
    else:
        post = get_object_or_404(Post.active_objects, slug=post_slug)
        template = "blog/article.html"
    lang_code = get_language()
    if not getattr(post, f"title_{lang_code}"):
        return redirect("homepage", permanent=True)
    categories = post.categories.values_list("slug", flat=True)
    return render(
        request,
        template,
        {"post": post, "categories": categories, "page_title": post.title},
    )
