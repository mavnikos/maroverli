from modeltranslation.translator import translator, TranslationOptions

from .models import (
    Category,
    Tag,
    Post,
    PostImageDeposit,
    PostTimeline,
    PostTimelineEntry,
)


class CategoryTranslate(TranslationOptions):
    fields = ("name", "header", "meta_description")


class TagTranslate(TranslationOptions):
    fields = ("name",)


class PostImageDepositTranslate(TranslationOptions):
    fields = ("alt_text",)


class PostTranslate(TranslationOptions):
    fields = (
        "title",
        "content",
        "short_content",
        "meta_description",
        "alt_text",
    )


class PostTimelineTranslate(TranslationOptions):
    fields = ("title", "meta_description")


class PostTimelineEntryTranslate(TranslationOptions):
    fields = ("heading", "content", "alt_text")


translator.register(Category, CategoryTranslate)
translator.register(Tag, TagTranslate)
translator.register(Post, PostTranslate)
translator.register(PostImageDeposit, PostImageDepositTranslate)
translator.register(PostTimeline, PostTimelineTranslate)
translator.register(PostTimelineEntry, PostTimelineEntryTranslate)
