from django.contrib import admin
from django.db.models import Count
from django.utils.html import format_html, mark_safe

from modeltranslation.admin import TranslationAdmin
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from django_summernote.admin import SummernoteModelAdmin

from dtl_utils.templatetags.dtl_tags import LANG_CODES
from .forms import PostAdminForm
from .admin_filters import HasShortContentListFilter
from .models import (
    Tag,
    Category,
    Post,
    PostImageDeposit,
    PostTimeline,
    PostTimelineEntry,
)


@admin.register(Tag)
class TagAdmin(TranslationAdmin):
    list_display = ["name"]
    fields = ["name"]


@admin.register(Category)
class CategoryAdmin(TranslationAdmin):
    list_display = ["name"]
    fields = [
        ("name_en", "name_el"),
        ("header_en", "header_el"),
        "meta_description",
        "social_photo",
    ]


class PostImageDepositInline(admin.StackedInline):
    model = PostImageDeposit
    fields = ("order", "large_img", ("alt_text_en", "alt_text_el"))
    extra = 1


class PostTimelineEntryInline(admin.StackedInline):
    model = PostTimelineEntry
    fields = (
        "order",
        ("heading_en", "heading_el"),
        "created_at",
        "updated_at",
        "content_en",
        "content_el",
        "image",
        "icon",
        "alt_text_en",
        "alt_text_el",
    )
    extra = 1


@admin.register(Post)
class PostAdmin(SummernoteModelAdmin, TranslationAdmin):
    form = PostAdminForm
    summernote_fields = ("content_en", "content_el")
    save_on_top = True
    save_as = True
    inlines = [PostImageDepositInline]
    list_display = [
        "show_thumbnail",
        "title_en",
        "title_el",
        "show_categories",
        "show_tags",
        "show_photo_count",
        "show_post_url",
        "created_at"
    ]
    list_display_links = ("show_thumbnail", "title_en", "title_el")
    readonly_fields = ("show_thumbnail", "permalink", "show_post_url")
    search_fields = ["title_en", "title_el"]
    list_filter = [
        "tags",
        "categories",
        HasShortContentListFilter,
        "created_at",
        "updated_at",
    ]
    filter_horizontal = ("tags", "categories")
    prepopulated_fields = {"slug": ("title_en",)}
    fields = [
        ("is_active", "order"),
        ("created_at", "updated_at"),
        ("title_en", "title_el"),
        ("slug", "permalink"),
        ("content_en", "content_el"),
        ("short_content_en", "short_content_el"),
        ("alt_text_en", "alt_text_el"),
        ("meta_description_en", "meta_description_el"),
        "categories",
        "tags",
        "thumbnail",
        "background_image",
        "pdf_file",
    ]

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.annotate(
            _image_count=Count("images", distinct=True)
        )
        return queryset

    def show_thumbnail(self, obj):
        src = obj.thumbnail.url
        return format_html(f"<img src={src} width=90px>")

    show_thumbnail.short_description = "Preview"

    def show_categories(self, obj):
        categories = list(obj.categories.values_list("name", flat=True))
        return ", ".join(categories)

    show_categories.short_description = "Categories"

    def show_tags(self, obj):
        t = list(obj.tags.values_list("name", flat=True))
        return ", ".join(t)

    show_tags.short_description = "Tags"

    def show_photo_count(self, obj):
        return obj._image_count

    show_photo_count.short_description = "No. Pics"
    show_photo_count.admin_order_field = "_image_count"

    def show_post_url(self, obj):
        buttons = []
        for lang_code in LANG_CODES:
            buttons.append(
                f"<button><a href='/{lang_code}{obj.permalink()}'>{lang_code}</a></button>"
            )
        buttons = "<br>".join(buttons)
        return mark_safe(buttons)

    show_post_url.short_description = "URL"


@admin.register(PostTimeline)
class PostTimelineAdmin(SummernoteModelAdmin, TranslationAdmin):
    summernote_fields = ("content_en", "content_el")
    save_on_top = True
    inlines = [PostTimelineEntryInline]
    list_display = ["show_thumbnail", "title_en", "title_el"]
    list_display_links = ("show_thumbnail", "title_en", "title_el")
    readonly_fields = ("show_thumbnail", "post_permalink")
    list_filter = ["created_at", "updated_at"]
    filter_horizontal = ("tags", "categories")
    prepopulated_fields = {"slug": ("title_en",)}
    fields = [
        ("created_at", "updated_at"),
        ("title_en", "title_el"),
        ("slug", "post_permalink"),
        "thumbnail",
        ("meta_description_en", "meta_description_el"),
        "categories",
        "tags",
    ]

    def show_thumbnail(self, obj):
        src = obj.thumbnail.url
        return format_html(f"<img src={src} width=90px>")

    show_thumbnail.short_description = "Preview"

    def post_permalink(self, obj):
        if obj.pk:
            return obj.get_absolute_url()[3:]  # strip out "/en" or "/el"
        return ""
