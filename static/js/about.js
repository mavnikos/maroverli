$(document).ready(function(){
	const cards = $(".about__box .card");
	const speed = 200;
	const endTime = (cards.length * speed) + 1000;
	const flip = function(el) {
		el.toggleClass("is-flipped");
	};
	// Flip cards ("about me" page)
	cards.each(function(index) {
		const timer = (index + 1) * speed;
		setTimeout(flip, timer, $(this));
		$(this).on("click", function () {
			flip($(this));
		});

	});
	$(cards.get().reverse()).each(function(index) {
		const timer = endTime + ((index + 1) * speed);
		setTimeout(flip, timer, $(this));
	});

	// Make back of card scrollable
	const cardsBack = $(".about__box .back");
	const simpleBarOptions = {autoHide: false};
	cardsBack.each(function() {
		new SimpleBar($(this)[0], simpleBarOptions);
	});

	// Scroll timeline ("about me" page)
	$(".about__bottom__container").flickity({
		contain: false,
		pageDots: false,
		freeScroll: true,
		groupCells: '100%',
	});
});
