$(document).ready(function(){
	// Horizontal scroll photo gallery
	// const galleryWrapper = ".article__gallery__wrapper";
	// const prevBtn = $(".article__gallery .gallery__prev__btn");
	// const nextBtn = $(".article__gallery .gallery__next__btn");
	// const scrollBy = 300;
	// nextBtn.on("click", function() {
	// 	document.querySelector(galleryWrapper).scrollBy({left: scrollBy});
	// });
	// prevBtn.on("click", function() {
	// 	document.querySelector(galleryWrapper).scrollBy({left: -scrollBy});
	// });

	  let slider = tns({
		  container: '.article__gallery__wrapper',
		  // items: 3,
		  slideBy: 1,
		  autoplay: false,
		  gutter: 20,
		  lazyload: true,
		  center: true,
		  controls: true,
		  controlsText: [
		  	"<svg class=\"arrow\" viewBox=\"0 0 100 100\"><path d=\"M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z\" fill=\"#ec2525\"></path></svg>",
		    "<svg class=\"arrow\" viewBox=\"0 0 100 100\"><path d=\"M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z\" fill=\"#ec2525\" transform=\"translate(100, 100) rotate(180) \"></path></svg>"
		  ],
		  nav: false,
		  speed: 900,
		  responsive: {
			  360: {
			  	items: 1,
			  },
			  900: {
				items: 2,
			  },
			  // 1200: {
				// items: 2,
			  // }
		  }
	  });


	// Enable lightbox
	let lb = $(".article__gallery a").simpleLightbox({
		alertError: false,
	});
	// lb.on("show.simplelightbox", function(e) {
	// 	e.target.blur();
	// 	e.target.parentElement.style.overflowX = "hidden";
	// });
	// lb.on("close.simplelightbox", function(e) {
	// 	e.target.parentElement.style.overflowX = "auto";
	// });
});
