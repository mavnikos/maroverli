$(document).ready(function(){
	// Intro effect (global)
	const introElem = $('.intro-effect');
	const pageContainer = $(".page__container");
	if (window.sessionStorage.getItem("introEffectRun") !== "1") {
		introElem.css("display", "flex").addClass("active");
		pageContainer.css("display", "grid").css("overflow", "hidden");
		introElem.on("webkitAnimationEnd oAnimationEnd msAnimationEnd animationend", function () {
			$(this).fadeOut(500);
			window.sessionStorage.setItem("introEffectRun", "1");
			pageContainer.css("overflow", "auto");
		});
	} else {
		pageContainer.css("display", "grid");
	}

	// Toggle menu overlay (global)
	const closeOverlayMenu = function(){
		$(".content__wrapper").show(0);
		$(".nav__menu__overlay").fadeOut(300);
		$("body").toggleClass("no-scroll");
	};
	const flip = function(el) {
		el.toggleClass("is-flipped");
	};

	$(".hamburger__menu").on("click keypress", function(e) {
		if (e.type === "click" || e.which === 13){
			$(".nav__menu__overlay").fadeIn(300);
			$("body").toggleClass("no-scroll").keyup(function(e) {
				if (e.which === 27) {closeOverlayMenu();}
			});
			const logoFlip = $(".logo.flip__box .card");
			setTimeout(flip, 1000, logoFlip);
			logoFlip.on("click", function () {
				flip($(this));
			});
			setTimeout(flip, 3000, logoFlip);
		}
	});
	$(".button__close").on("click keypress", function(e) {
		if (e.type === "click" || e.which === 13){
			closeOverlayMenu();
		}
	});

	// Lazy load images
	// const clsName = "gallery__image";
	// const callback_reveal = function(el) {
	// 	if ($(el).hasClass(clsName)) {
	// 		$(el).before('<div class="spinner"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" version="1.1" y="0px" x="0px" viewBox="-150 -70 300 300"><path id="half-circle" d="m0 0.9882034c54.66894 0 99.0118 44.32368 99.0118 99.02414 0 54.70047-44.34286 98.99945-99.0118 98.99945" stroke="#ec2525" stroke-width="5" fill="none"/></svg></div>');
	// 	}
	// };
	// const callback_loaded = function(el){
	// 	if ($(el).hasClass(clsName)) {
	// 		$(el).prev().hide();
	// 		$(el).fadeTo(500, 1);
	// 	}
	// };
	const lazyLoadInstance = new LazyLoad({
		elements_selector: ".lazy",
		// callback_reveal: callback_reveal,
		// callback_loaded: callback_loaded,
	});
});
