$(document).ready(function(){
	let posts = $('.portfolio__content__posts');
	let thumbs = $(".post__preview__wrapper");
	let options = {
		gutterPixels: 20,
		callbacks: {
			onInit: function() {
				$(".portfolio__spinner").fadeOut();
			}
		}
	};
	let calcMargin = function() {
		// Center portfolio
		let thumbsPerRow = Math.floor(posts.width() / (thumbs.width() + options.gutterPixels));
		let rowWidth = (thumbsPerRow * thumbs.width()) + (options.gutterPixels * (thumbsPerRow - 1));
		let margin = Math.floor((posts.width() - rowWidth) / 2);
		posts.css("margin-left", margin);
	};

	posts.imagesLoaded().done(function() {
		let fz = posts.filterizr(options);
		const activeClass = "active";
		calcMargin();
		$(window).on("resize", calcMargin);
		$(".tags__container .tag").on("click", function () {
			$(".tags__container .tag").removeClass(activeClass);
			$(this).addClass(activeClass);
			fz.filterizr("filter", $(this).data("filter"));
		});
	});
});
