from django.db import models


class MediaPublicity(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField("Content", blank=True)
    image = models.ImageField(
        upload_to="media_publicity/images/",
        blank=True,
        null=True,
        help_text="Maximum width: 500px. Height: depends on width.",
    )
    pdf = models.FileField(
        verbose_name="PDF file",
        upload_to="media_publicity/files/",
        blank=True,
        null=True,
        help_text="Only PDF format files.",
    )
    video_url = models.URLField(
        "Video URL",
        blank=True,
        null=True,
        help_text="Example: https://www.youtube.com/embed/cY13DPpk__Q<br>or "
        "https://www.dailymotion.com/embed/video/x16toyn.<br>"
        "URL <strong>must include the 'embed' keyword.</strong>",
    )
    order = models.SmallIntegerField(default=0)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Media Publicity Entries"
        ordering = ["order", "title"]
