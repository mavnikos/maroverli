from modeltranslation.translator import translator, TranslationOptions

from .models import MediaPublicity


class MediaPublicityTranslate(TranslationOptions):
    fields = ("title", "text")


translator.register(MediaPublicity, MediaPublicityTranslate)
