from django.contrib import admin
from django.utils.html import format_html

from modeltranslation.admin import TranslationAdmin
from adminsortable2.admin import SortableAdminMixin
from django_summernote.admin import SummernoteModelAdmin

from .models import MediaPublicity
from .forms import MediaPublicityForm


@admin.register(MediaPublicity)
class MediaPublicityAdmin(
    SummernoteModelAdmin, SortableAdminMixin, TranslationAdmin
):
    form = MediaPublicityForm
    summernote_fields = ["text"]
    list_display = ["title"]
    fields = [
        ("title_en", "title_el"),
        ("text_en", "text_el"),
        ("show_image", "image"),
        "pdf",
        "video_url",
    ]
    readonly_fields = ["show_image"]
    save_on_top = True

    def show_image(self, obj):
        src = obj.image.url
        return format_html(f"<img src={src} width=100px>")

    show_image.short_description = "Preview"
