from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from .models import MediaPublicity


def index(request):
    medias = MediaPublicity.objects.all()
    page_title = "Media Publicity"
    meta_description = _("If you have a photo request, send me an email or "
                         "have a look here for media coverage, interviews, "
                         "press & social media mentions, and other shoutouts.")
    return render(request, "media_publicity.html", {
        "medias": medias,
        "page_title": page_title,
        "meta_description": meta_description,
    })
