from django import forms

from filetype import guess
from filetype.types.archive import Pdf


class MediaPublicityForm(forms.ModelForm):
    def clean_pdf_file(self):
        data = self.cleaned_data["pdf_file"]
        t = guess(data.file)
        if not isinstance(t, Pdf):
            raise forms.ValidationError('Only PDF file types allowed!')
        return data

    def clean_video_url(self):
        data = self.cleaned_data["video_url"]
        if data is not None and "embed" not in data:
            err = "Video URL is not a valid format. Please copy the link" \
                  " from the embed button. The URL must include the " \
                  "word \"embed\"!"
            raise forms.ValidationError(err)
        return data

    def clean(self):
        cleaned_data = super().clean()
        image = cleaned_data.get("image")
        pdf = cleaned_data.get("pdf")
        video_url = cleaned_data.get("video_url")
        if [bool(image), bool(pdf), bool(video_url)].count(True) > 1:
            raise forms.ValidationError("Only one of Image/PDF/VideoURL should be non-empty.")
        return cleaned_data
