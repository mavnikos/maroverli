from django.apps import AppConfig


class MediaPublicityConfig(AppConfig):
    name = 'media_publicity'
    verbose_name = "Media Publicity"
