import io
import json


def get_countries_obj(path="travel_map/country_codes.json"):
    return json.load(io.open(path, encoding="utf-8-sig"))


def get_coordinates(numeric_code=None):
    if numeric_code is not None:
        countries = json.load(
            io.open("travel_map/country_codes.json", encoding="utf-8-sig")
        )
        for country in countries:
            if str(country["num_code"]) == str(numeric_code):
                return country["latitude"], country["longitude"]
    return 0, 0
