from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from .models import Point


def index(request):
    points = Point.objects.all()

    data = [
        {
            "country": point.country.name,
            "coordinates": [str(point.latitude), str(point.longitude)],
            "postData": [
                {
                    "postTitle": post.title,
                    "postUrl": post.get_absolute_url(),
                    "postThumbnailUrl": post.thumbnail.url,
                    "postYear": post.updated_at.year,
                }
                for post in point.posts.all()
            ],
        }
        for point in points
    ]
    meta_description = _("Traveled in 30 countries & overnight in many "
                         "airports. Traveling is an opportunity for "
                         "self-development by exploring your limits & being "
                         "open to the unknown.")
    page_title = _("Travel Map")
    return render(
        request, "travel_map/index.html", {
            "points": points,
            "data": data,
            "meta_description": meta_description,
            "page_title": page_title,
        }
    )
