from django.contrib import admin

from adminsortable2.admin import SortableAdminMixin

from .models import Point
from .utils import get_coordinates


@admin.register(Point)
class PointAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ["country"]
    fields = [
        "country",
        ("latitude", "longitude"),
        "posts",
    ]
    readonly_fields = ["latitude", "longitude"]
    filter_horizontal = ("posts",)

    def save_model(self, request, obj, form, change):
        obj.latitude, obj.longitude = get_coordinates(obj.country.numeric)
        super().save_model(request, obj, form, change)

    class Media:
        css = {"all": ("css/admin/select2-v4.0.10.min.css",)}
        js = ("js/admin/select2-v4.0.10.min.js", "js/admin/my_select2.js")
