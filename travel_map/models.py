from django.db import models

from django_countries.fields import CountryField

from blog.models import Post


class Point(models.Model):
    country = CountryField()
    latitude = models.DecimalField(max_digits=8, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    order = models.PositiveSmallIntegerField(default=0)
    posts = models.ManyToManyField(Post)

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return self.country.name
