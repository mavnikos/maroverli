from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from .models import (
    TimelineEntry,
    AboutProfile,
    AboutScopeOfWork,
    AboutSkill,
    AboutExperience,
    AboutExperienceLanguage,
    AboutEducation,
    AboutAchievement,
    AboutFact,
)


def index(request):
    timeline_entries = TimelineEntry.objects.filter(is_active=True)
    profile = AboutProfile.objects.first()
    scopes = AboutScopeOfWork.objects.all()
    skills = AboutSkill.objects.all()
    experiences = AboutExperience.objects.all()
    languages = AboutExperienceLanguage.objects.all()
    educations = AboutEducation.objects.all()
    achievements = AboutAchievement.objects.all()
    facts = AboutFact.objects.all()
    meta_description = _("Dare to Dream Big! Cause Every Big dream is a new "
                         "goal and every difficulty, an opportunity to learn. "
                         "Here is my life story! "
                         "Available for new challenges!")
    page_title = _("About Me")
    return render(
        request,
        "about/index.html",
        {
            "timeline_entries": timeline_entries,
            "profile": profile,
            "scopes": scopes,
            "skills": skills,
            "experiences": experiences,
            "languages": languages,
            "educations": educations,
            "achievements": achievements,
            "facts": facts,
            "meta_description": meta_description,
            "page_title": page_title,
        },
    )
