from modeltranslation.translator import translator, TranslationOptions

from .models import (
    TimelineEntry,
    AboutCommonProfile,
    AboutCommonScopeOfWork,
    AboutCommonSkill,
    AboutCommonExperience,
    AboutCommonEducation,
    AboutCommonAchievement,
    AboutCommonFact,
    AboutProfile,
    AboutScopeOfWork,
    AboutSkill,
    AboutExperience,
    AboutExperienceLanguage,
    AboutEducation,
    AboutAchievement,
    AboutFact,
)


class TimelineEntryTranslate(TranslationOptions):
    fields = ("text",)


class AboutCommonTranslate(TranslationOptions):
    fields = ("header", "letter_image")


class AboutCommonProfileTranslate(AboutCommonTranslate):
    pass


class AboutCommonScopeOfWorkTranslate(AboutCommonTranslate):
    pass


class AboutCommonSkillTranslate(AboutCommonTranslate):
    pass


class AboutCommonExperienceTranslate(AboutCommonTranslate):
    pass


class AboutCommonEducationTranslate(AboutCommonTranslate):
    pass


class AboutCommonAchievementTranslate(AboutCommonTranslate):
    pass


class AboutCommonFactTranslate(AboutCommonTranslate):
    pass


class ProfileTranslate(TranslationOptions):
    fields = ("text",)


class ScopeOfWorkTranslate(TranslationOptions):
    fields = ("text",)


class SkillTranslate(TranslationOptions):
    fields = ("skill",)


class ExperienceTranslate(TranslationOptions):
    fields = ("year", "place", "job_title", "job_employer")


class ExperienceLanguageTranslate(TranslationOptions):
    fields = ("text",)


class EducationTranslate(TranslationOptions):
    fields = ("years", "title", "university")


class AchievementTranslate(TranslationOptions):
    fields = ("text",)


class FactTranslate(TranslationOptions):
    fields = ("text",)


translator.register(TimelineEntry, TimelineEntryTranslate)

translator.register(AboutCommonProfile, AboutCommonProfileTranslate)
translator.register(AboutProfile, ProfileTranslate)

translator.register(AboutCommonScopeOfWork, AboutCommonScopeOfWorkTranslate)
translator.register(AboutScopeOfWork, ScopeOfWorkTranslate)

translator.register(AboutCommonSkill, AboutCommonSkillTranslate)
translator.register(AboutSkill, SkillTranslate)

translator.register(AboutCommonExperience, AboutCommonExperienceTranslate)
translator.register(AboutExperience, ExperienceTranslate)

translator.register(AboutExperienceLanguage, ExperienceLanguageTranslate)

translator.register(AboutCommonEducation, AboutCommonEducationTranslate)
translator.register(AboutEducation, EducationTranslate)

translator.register(AboutCommonAchievement, AboutCommonAchievementTranslate)
translator.register(AboutAchievement, AchievementTranslate)

translator.register(AboutCommonFact, AboutCommonFactTranslate)
translator.register(AboutFact, FactTranslate)
