from django.contrib import admin
from django.utils.html import format_html

from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from django_summernote.admin import SummernoteModelAdmin, SummernoteInlineModelAdmin

from .models import (
    TimelineEntry,
    AboutCommonProfile,
    AboutCommonScopeOfWork,
    AboutCommonSkill,
    AboutCommonExperience,
    AboutCommonEducation,
    AboutCommonAchievement,
    AboutCommonFact,
    AboutProfile,
    AboutScopeOfWork,
    AboutSkill,
    AboutExperience,
    AboutExperienceLanguage,
    AboutEducation,
    AboutAchievement,
    AboutFact,
)


@admin.register(TimelineEntry)
class TimelineEntryAdmin(
    SummernoteModelAdmin, SortableAdminMixin, TranslationAdmin
):
    summernote_fields = ("text",)
    list_display = ["order", "year", "shorten_text"]
    list_display_links = ("year",)
    fields = ["is_active", ("icon", "year"), ("text",)]


class ProfileAdminInline(TranslationStackedInline):
    model = AboutProfile
    fields = ("text", "about")
    min_num = 1
    max_num = 1


class ScopeOfWorkAdminInline(
    SortableInlineAdminMixin, TranslationStackedInline
):
    model = AboutScopeOfWork
    fields = ("icon", "text", "about")
    min_num = 1
    extra = 1


class SkillAdminInline(SortableInlineAdminMixin, TranslationStackedInline):
    model = AboutSkill
    fields = ("skill", "percentage", "about")
    min_num = 1
    extra = 1


class ExperienceAdminInline(SortableInlineAdminMixin, TranslationStackedInline):
    model = AboutExperience
    fields = ("year", "place", "job_title", "job_employer", "about")
    min_num = 1
    extra = 1


class EducationAdminInline(SortableInlineAdminMixin, TranslationStackedInline):
    model = AboutEducation
    fields = ("years", "title", "university", "icon", "about")
    min_num = 1
    extra = 1


class AchievementAdminInline(
    SummernoteInlineModelAdmin, SortableInlineAdminMixin, TranslationStackedInline
):
    model = AboutAchievement
    summernote_fields = ("text_en", "text_el")
    fields = ("icon", "text", "about")
    min_num = 1
    extra = 1


class FactAdminInline(
    SortableInlineAdminMixin, TranslationStackedInline
):
    model = AboutFact
    fields = ("icon", "text", "about")
    min_num = 1
    extra = 1


class AboutCommonAdmin(TranslationAdmin):
    list_display = ["show_letter"]
    fields = ["letter", ("show_letter", "letter_image"), "header"]
    readonly_fields = ["show_letter"]

    def has_add_permission(self, request):
        return not self.model.objects.count()

    def show_letter(self, obj):
        src = obj.letter_image.url
        return format_html(f"<img src={src} width=100px>")

    show_letter.short_description = "Preview"


@admin.register(AboutCommonProfile)
class AboutCommonProfileAdmin(AboutCommonAdmin):
    save_on_top = True
    inlines = [ProfileAdminInline]


@admin.register(AboutCommonScopeOfWork)
class AboutScopeOfWorkAdmin(AboutCommonAdmin):
    save_on_top = True
    inlines = [ScopeOfWorkAdminInline]


@admin.register(AboutCommonSkill)
class AboutSkillAdmin(AboutCommonAdmin):
    save_on_top = True
    inlines = [SkillAdminInline]


@admin.register(AboutCommonExperience)
class AboutExperienceAdmin(AboutCommonAdmin):
    save_on_top = True
    inlines = [ExperienceAdminInline]


@admin.register(AboutExperienceLanguage)
class AboutExperienceLanguageAdmin(SortableAdminMixin, TranslationAdmin):
    pass


@admin.register(AboutCommonEducation)
class AboutEducationAdmin(AboutCommonAdmin):
    save_on_top = True
    inlines = [EducationAdminInline]


@admin.register(AboutCommonAchievement)
class AboutAchievementAdmin(AboutCommonAdmin):
    save_on_top = True
    inlines = [AchievementAdminInline]


@admin.register(AboutCommonFact)
class AboutFactAdmin(AboutCommonAdmin):
    save_on_top = True
    inlines = [FactAdminInline]
