from textwrap import shorten

from django.db import models
from django.utils.translation import get_language
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils.html import strip_tags
from django.utils.translation import ugettext_lazy as _


class TimelineEntry(models.Model):
    is_active = models.BooleanField(default=True)
    year = models.CharField(max_length=4)
    icon = models.CharField(
        max_length=30,
        help_text="Go to <a href='https://feathericons.com'>feather icons</a> "
        "and enter the name of the icon you want.<br>Example: if "
        "you want the alert-circle icon, simply write alert-circle.",
    )
    text = models.TextField()
    order = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name = "Timeline Entry"
        verbose_name_plural = "Timeline Entries"
        ordering = ["order"]

    def __str__(self):
        return self.year

    def shorten_text(self):
        return strip_tags(shorten(self.text.strip(), width=150, placeholder="..."))

    def icon_path(self):
        return static(f"img/icons/svg/{self.icon.strip()}.svg")


class AboutCommonABC(models.Model):

    LETTER_A = "A"
    LETTER_B = "B"
    LETTER_O = "O"
    LETTER_U = "U"
    LETTER_T = "T"
    LETTER_M = "M"
    LETTER_E = "E"

    LETTER_CHOICES = (
        (LETTER_A, _("A")),
        (LETTER_B, _("B")),
        (LETTER_O, _("O")),
        (LETTER_U, _("U")),
        (LETTER_T, _("T")),
        (LETTER_M, _("M")),
        (LETTER_E, _("E")),
    )

    header = models.CharField(max_length=30)
    letter = models.CharField(max_length=1, choices=LETTER_CHOICES)
    letter_image = models.ImageField(
        upload_to="img/letters", help_text="Image height must be 250px."
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.header

    def pre_letter(self):
        return f"{self.letter}_{get_language().upper()}"

    def get_img_url(self):
        return self.letter_image.url


class AboutCommonProfile(AboutCommonABC):
    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"


class AboutProfile(models.Model):
    text = models.TextField()
    about = models.ForeignKey(
        AboutCommonProfile, on_delete=models.CASCADE, related_name="profiles"
    )

    def __str__(self):
        return shorten(self.text, width=50, placeholder="...")


class AboutCommonScopeOfWork(AboutCommonABC):
    class Meta:
        verbose_name = "Scope of Work"
        verbose_name_plural = "Scopes of Work"


class AboutScopeOfWork(models.Model):
    text = models.TextField()
    icon = models.CharField(
        max_length=30,
        help_text="Go to <a href='https://feathericons.com'>feather icons</a> "
        "and enter the name of the icon you want.<br>Example: if "
        "you want the alert-circle icon, simply write alert-circle.",
    )
    order = models.PositiveSmallIntegerField(default=0)
    about = models.ForeignKey(
        AboutCommonScopeOfWork,
        on_delete=models.CASCADE,
        related_name="scopes_of_work",
    )

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return shorten(self.text, width=30, placeholder="...")

    def icon_path(self):
        return static(f"img/icons/svg/{self.icon.strip()}.svg")


class AboutCommonSkill(AboutCommonABC):
    class Meta:
        verbose_name = "Skill"
        verbose_name_plural = "Skills"


class AboutSkill(models.Model):
    skill = models.CharField(max_length=50)
    percentage = models.PositiveSmallIntegerField(
        help_text="A number between 0 - 100."
    )
    order = models.PositiveSmallIntegerField(default=0)
    about = models.ForeignKey(
        AboutCommonSkill, on_delete=models.CASCADE, related_name="skills"
    )

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return self.skill


class AboutCommonExperience(AboutCommonABC):
    class Meta:
        verbose_name = "Experience"
        verbose_name_plural = "Experiences"


class AboutExperience(models.Model):
    year = models.CharField(max_length=20)
    place = models.CharField(max_length=50)
    job_title = models.CharField(max_length=100)
    job_employer = models.CharField(max_length=70)
    order = models.PositiveSmallIntegerField(default=0)
    about = models.ForeignKey(AboutCommonExperience, on_delete=models.CASCADE,
                              related_name="experiences")

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return f"{self.year} | {self.place}"


class AboutExperienceLanguage(models.Model):
    text = models.CharField(max_length=50)
    percentage = models.PositiveSmallIntegerField(
        help_text="A number between 0 - 100."
    )
    order = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name = "lLnguage"
        verbose_name_plural = "Languages"
        ordering = ["order"]

    def __str__(self):
        return self.text


class AboutCommonEducation(AboutCommonABC):
    class Meta:
        verbose_name = "Education"
        verbose_name_plural = "Educations"


class AboutEducation(models.Model):
    years = models.CharField(max_length=20)
    title = models.CharField(max_length=100)
    university = models.CharField(max_length=80)
    icon = models.CharField(
        max_length=30,
        help_text="Go to <a href='https://feathericons.com'>feather icons</a> "
                  "and enter the name of the icon you want.<br>Example: if "
                  "you want the alert-circle icon, simply write alert-circle.",
    )
    order = models.PositiveSmallIntegerField(default=0)
    about = models.ForeignKey(AboutCommonEducation, on_delete=models.CASCADE,
                              related_name="educations")

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return self.title

    def icon_path(self):
        return static(f"img/icons/svg/{self.icon.strip()}.svg")


class AboutCommonAchievement(AboutCommonABC):
    class Meta:
        verbose_name = "Achievement"
        verbose_name_plural = "Achievements"


class AboutAchievement(models.Model):
    text = models.TextField()
    icon = models.CharField(
        max_length=30,
        help_text="Go to <a href='https://feathericons.com'>feather icons</a> "
                  "and enter the name of the icon you want.<br>Example: if "
                  "you want the alert-circle icon, simply write alert-circle.",
    )
    order = models.PositiveSmallIntegerField(default=0)
    about = models.ForeignKey(AboutCommonAchievement, on_delete=models.CASCADE,
                              related_name="achievements")

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return shorten(self.text, width=50, placeholder="...")

    def icon_path(self):
        return static(f"img/icons/svg/{self.icon.strip()}.svg")


class AboutCommonFact(AboutCommonABC):
    class Meta:
        verbose_name = "Facts"
        verbose_name_plural = "Facts"


class AboutFact(models.Model):
    text = models.TextField()
    icon = models.CharField(
        max_length=30,
        help_text="Go to <a href='https://feathericons.com'>feather icons</a> "
        "and enter the name of the icon you want.<br>Example: if "
        "you want the alert-circle icon, simply write alert-circle.",
    )
    order = models.PositiveSmallIntegerField(default=0)
    about = models.ForeignKey(
        AboutCommonFact,
        on_delete=models.CASCADE,
        related_name="scopes_of_work",
    )

    class Meta:
        verbose_name = "Fact"
        verbose_name_plural = "Facts"
        ordering = ["order"]

    def __str__(self):
        return shorten(self.text, width=30, placeholder="...")

    def icon_path(self):
        return static(f"img/icons/svg/{self.icon.strip()}.svg")
